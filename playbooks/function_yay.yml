#!/usr/bin/env ansible-playbook
#
# === Install AUR packages via yay ===
#
# Invoke like so (no indention nor inside of task block):
#
# - name: Install AUR pkgs
#   vars:
#     aur_packages:
#       - package1
#       - package2
#       ...
#     aur_packages_state: present|latest  # pick only one to install/uninstall
#   ansible.builtin.import_playbook: function_yay.yml
#
# LIMITATIONS:
# 'absent' for >>aur_packages_state<< doesn't appear to work
#
# PREREQUISITES:
# `ansible-yay` module installed into plugins/modules folder
#
# Reference:
# https://github.com/mnussbaum/ansible-yay
#
# NOTE:
# This playbook temporarily adds/removes a passwordless sudo ansible_user
# when using yay module since yay cannot be ran with sudo
# but will require (and prompt for sudoer password) later.
# This is a pretty egrious security hole, but the playbook is written such
# that it should always remove that hole after packages are installed, removed
# or failed.
#
# Reference:
# https://github.com/kewlfft/ansible-aur#create-the-aur_builder-user
---
- name: '=== [AUR_FUNCTION_START] Add/Remove >>AUR<< packages ==='
  hosts: all
  vars:
    passwordless_sudo_user: '{{ ansible_user }}'
    passwordless_sudo_file: /etc/sudoers
    passwordless_sudo_program: /usr/bin/pacman
    aur_packages:
      - package1
      - package2
      - package3
    aur_packages_state: present
  tasks:
    - name: When using Arch Linux block
      when: ansible_facts["distribution"] == "Archlinux"
      block:

        - name: Ensure yay is installed (ignore errors)
          become: true
          ignore_errors: true
          changed_when: false
          ansible.builtin.package:
            name:
              - yay-bin
              - base-devel
            state: present
          register: is_yay_installed

        - name: 'Add `{{ passwordless_sudo_user }}` user to sudoers enabling passwordless `sudo {{ passwordless_sudo_program }}`'
          become: true
          changed_when: false
          ansible.builtin.lineinfile:
            path: '{{ passwordless_sudo_file }}'
            line: '{{ passwordless_sudo_user }} ALL=(ALL) NOPASSWD: {{ passwordless_sudo_program }}'
            create: true
            validate: 'visudo --check --strict --file=%s'
            mode: '0440'
            state: present

        - name: Ensure `yay` (AUR) pkg manager is installed
          when: is_yay_installed is failed
          vars:
            - yay_dir: /tmp/yay-bin/
          block:

            - name: Ensure yay deps are installed
              become: true
              ansible.builtin.package:
                name:
                  - git
                  - base-devel
                state: present

            - name: Create git dir
              ansible.builtin.file:
                path: '{{ yay_dir }}'
                state: directory
                mode: '0755'

            - name: Clone yay repo
              ansible.builtin.git:
                repo: https://aur.archlinux.org/yay-bin.git
                dest: '{{ yay_dir }}'
                clone: true
                update: true
                force: true

            - name: Install yay
              ansible.builtin.command:
                chdir: '{{ yay_dir }}'
                cmd: makepkg --syncdeps --install --force --noconfirm
                creates: /usr/bin/yay

        - name: 'When package is absent'
          when: '"absent" in aur_packages_state'
          ansible.builtin.fail:
            msg:
              - '"absent" as state for `aur_packages_state` does NOT work'
              - 'Uninstall with pacman normally'

        - name: 'Ensure {{ aur_packages }} pkgs are: {{ aur_packages_state }}'
          kewlfft.aur.aur:
            name:
              - '{{ item }}'
            state: '{{ aur_packages_state }}'
          loop: '{{ aur_packages }}'

      # Always removes this temp security hole even with a previous failure
      always:
        - name: === [AUR_FUNCTION_END] Cleanup `{{ passwordless_sudo_user }}` user from `{{ passwordless_sudo_file }}` ===
          become: true
          changed_when: false
          ansible.builtin.lineinfile:
            path: '{{ passwordless_sudo_file }}'
            line: '{{ passwordless_sudo_user }} ALL=(ALL) NOPASSWD: {{ passwordless_sudo_program }}'
            validate: 'visudo --check --strict --file=%s'
            state: absent
