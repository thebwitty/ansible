#!/usr/bin/env ansible-playbook
#
# Various fixes (usually from Proton DB) for Steam Linux games
#
# References:
# - https://wiki.archlinux.org/title/Steam#Proton_Steam-Play
# - Civ5 launch options: https://www.protondb.com/app/8930#Umnb6VrvAR
# - Civ6 launch options: https://www.protondb.com/app/289070#GP4yXYME6b
---
- name: Steam Linux compatibility fixes for games
  hosts: all

  tasks:

    # https://wiki.archlinux.org/title/Official_repositories#multilib
    - name: Enable pacman multilib
      become: true
      ansible.builtin.blockinfile:
        path: /etc/pacman.conf
        marker: "# {mark} pacman_multilib_enable"
        create: true
        state: present
        mode: '0644'
        block: |
          [multilib]
          Include = /etc/pacman.d/mirrorlist
      register: pacman_multilib_enabled

    - name: Upgrade system (required when enabling multilib)
      when: pacman_multilib_enabled.changed
      become: true
      ansible.builtin.package:
        update_cache: true
        upgrade: true

    # https://wiki.archlinux.org/title/Steam#Installation
    - name: Ensure steam and deps are installed
      become: true
      ansible.builtin.package:
        name:
          - lib32-nvidia-utils  # lib32-vulkan-driver: install before 'steam' pkg to prevent steam installing AMD drivers
          - nvidia-utils  # vulkan-driver: see previous line
          - nvidia  # Nvidia: closed-source GPU
          - lib32-systemd
          - wine
          - mesa  # Intel: open-source GPU
          - ttf-liberation
          - steam
        state: present

    - name: Ensure certain packages are not installed
      become: true
      ansible.builtin.package:
        name:
          - lib32-amdvlk
          - amdvlk
        state: absent

- name: Install AUR pkgs
  vars:
    aur_packages:
      - optimus-manager  # GPU switcher
  ansible.builtin.import_playbook: function_yay.yml

- name: Steam tweaks for GPU, Proton, and specific game issues
  hosts: all
  tasks:

    - name: Enable optimus-manager service
      become: true
      ansible.builtin.service:
        name: optimus-manager
        enabled: true
        state: started

    # https://github.com/GloriousEggroll/proton-ge-custom#native
    - name: === Proton GloriousEggroll Block ===
      vars:
        - proton_ge_version: GE-Proton7-49
        - proton_ge_filename: '{{ proton_ge_version }}.tar.gz'
        - proton_ge_url: https://github.com/GloriousEggroll/proton-ge-custom/releases/download/{{ proton_ge_version }}/{{ proton_ge_filename }}
        - install_location: ~/.steam/root/compatibilitytools.d/
        - download_location: '{{ install_location }}/{{ proton_ge_filename }}'
      block:

        - name: Ensure folder exists
          ansible.builtin.file:
            path: '{{ install_location }}'
            owner: '{{ ansible_user }}'
            group: '{{ ansible_user }}'
            mode: '0755'
            state: directory

        - name: 'Download Proton GE version: {{ proton_ge_version }}'
          ansible.builtin.get_url:
            url: '{{ proton_ge_url }}'
            dest: '{{ download_location }}'
            owner: '{{ ansible_user }}'
            group: '{{ ansible_user }}'
            mode: '0755'

        - name: Extract Proton GE archive
          ansible.builtin.unarchive:
            remote_src: true
            src: '{{ download_location }}'
            dest: '{{ install_location }}'
            owner: '{{ ansible_user }}'
            group: '{{ ansible_user }}'
            mode: '0755'
            creates: '{{ install_location }}/{{ proton_ge_version }}'

    # Multi Desync: https://www.protondb.com/app/813780#T4QkvqCK-9
    - name: === AOE2 Block ===
      vars:
        # Leave this so that the files get placed in the user dir and not /root
        - aoe2_dir: /home/{{ ansible_user }}/.local/share/Steam/steamapps/compatdata/813780/pfx/drive_c/windows/system32/
        - vc_redist_exe: vc_redist.x64.exe
        - vc_redist_url: https://download.microsoft.com/download/9/3/F/93FCF1E7-E6A4-478B-96E7-D4B285925B00/{{ vc_redist_exe }}
        - vulcan_folder: /usr/share/vulkan/icd.d/
      block:

        - name: Install necessary pkgs
          become: true
          ansible.builtin.package:
            name:
              - cabextract
            state: present

        - name: Ensure proper AOE2 folder exists
          ansible.builtin.file:
            path: '{{ aoe2_dir }}'
            state: directory
            mode: '0755'

        - name: Download vc_redist file
          ansible.builtin.get_url:
            url: '{{ vc_redist_url }}'
            dest: '{{ aoe2_dir }}{{ vc_redist_exe }}'
            owner: '{{ ansible_user }}'
            group: '{{ ansible_user }}'
            mode: '0755'
            checksum: sha256:5eea714e1f22f1875c1cb7b1738b0c0b1f02aec5ecb95f0fdb1c5171c6cd93a3

        # NOTE: cabextract requires elevation
        - name: Extract from downloaded file
          become: true
          ansible.builtin.command:
            cmd: cabextract {{ vc_redist_exe }}
            chdir: '{{ aoe2_dir }}'
            creates: a10

        # NOTE: cabextract requires elevation
        - name: Extract from extracted file
          become: true
          ansible.builtin.command:
            cmd: cabextract a10
            chdir: '{{ aoe2_dir }}'
            creates: ucrtbase.dll

        - name: Change ownership (chown) to normal user
          become: true
          ansible.builtin.file:
            path: '{{ aoe2_dir }}'
            owner: '{{ ansible_user }}'
            group: '{{ ansible_user }}'
            recurse: true
            state: directory

        # Crash after Splash: https://www.protondb.com/app/813780#eoQwQT6ngx
        - name: Move 1/2 file to fix crash after splash screen
          become: true
          ansible.builtin.command:
            cmd: mv intel_icd.x86_64.json intel_icd.x86_64.json.disabled
            chdir: '{{ vulcan_folder }}'
            removes: intel_icd.x86_64.json

        - name: Move 2/2 file to fix crash after splash screen
          become: true
          ansible.builtin.command:
            cmd: mv intel_icd.i686.json intel_icd.i686.json.disabled
            chdir: '{{ vulcan_folder }}'
            removes: intel_icd.i686.json
